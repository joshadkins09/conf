# general unix
alias rm='rm -i'
alias ssh='ssh -e :'
export LESS='-r'
export EDITOR='emacs'
export SVN_EDITOR='emacs'
export TERM=xterm-256color

# ls aliases
alias ls='ls -G'
alias la='ls -aG'
alias ll='ls -lG'
alias lla='ls -laG'

# grep aliases
alias egrep='egrep --color=always'
alias fgrep='fgrep --color=always'
alias zgrep='zgrep --color=always'
alias grep='grep --color=always'

# tmux aliases
alias tmux='tmux -2'
alias ta='tmux attach'
alias tl='tmux ls'

# emacs aliases
alias e='emacs -nw'
alias et='~/devel/conf/bin/ec -t'
alias ec='~/devel/conf/bin/ec'

# notification setup
export PUSHOVER_TOKEN=aGiB6qxHgPJog8Vt1vAM6Gxuf4D1wN
export PUSHOVER_USER=uMRuYkHDC6bhhkmQVHJFmnboasfjN3
export PUSHBULLET_TOKEN=Ua28Eau6opvLaYlbVUOEacA9CGsl9tp6

