#==============================================================================#
# Author: Josh Adkins                                                          #
# Title : .bashrc                                                              #
# Date  : 21 January 2013                                                      #
#==============================================================================#

[ -n "$TMUX" ] && export TERM=screen-256color
if [ -x /usr/bin/dircolors ]
then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# colors will work nice when used with solarized palette
color_off='\[\e[0m\]'       # Text Reset
white='\[\e[0;37m\]'        # White
bpurple='\[\e[1;35m\]'      # Purple
green='\[\e[0;32m\]'        # Green
blue='\[\e[0;34m\]'         # Blue
yellow='\[\e[0;33m\]'       # Yellow
purple='\[\e[0;35m\]'       # Purple
bwhite='\[\e[1;37m\]'       # White

PS1="${green}\u${color_off}@${blue}\h${color_off} ${bpurple}-->${color_off} "

export POWERLINE=$(pip show powerline-status | grep Location | awk '{print $NF}')/powerline
python -c "import powerline" > /dev/null
if [ $? -eq 0 ]; then
    powerline-daemon -q
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
    . $POWERLINE/bindings/bash/powerline.sh
fi

if [ -f $HOME/.customrc ]
then
    source $HOME/.customrc
fi
