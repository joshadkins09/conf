#!/usr/bin/python
'''
The purpose of this file is to provide an easy automated way for
installing unix con configuration files. Basically it checks first
to see if configuration files exist in the user's home directory,
checks if the user wants to save a copy of the old file, and
replaces it with the new file.

Instead of hardcoding or copying files, the new configration files
are symbolically linked to the actual files located in the cloned
git repository, providing an easy method of keeping files up to
date. With that in mind, if the absolute path to git repo changes,
the script will have to be re-run from the new location so the
location of the git repo should be thought about with a little bit
of care.
'''

from __future__ import print_function

import argparse
import os
import shutil
import sys

CONF_DIR = os.path.abspath(os.getcwd())  # absolute path to conf file repo
HOME_DIR = os.path.expanduser('~')  # user's home directory
CONF_FILES = [cf for cf in os.listdir(os.getcwd()) if cf.startswith('.')
              and 'git' not in cf]

if not CONF_FILES:
    sys.exit('no configuration files found in current directory')

def check_with_user(conf):
    '''
    If there already is a conf file with the same name as the one the
    user is trying to add to their home directory, this function asks
    the user whether they would like to save a copy of their before
    applying the new configuration file. The new name of the file will
    be the original name of the file with the extension '.orig' appended
    to it.
    '''
    target = os.path.join(HOME_DIR, conf)
    res = ''
    while not res.lower().startswith('y') and not res.lower().startswith('n'):
        res = raw_input('Save your {} file before overwriting? '.format(conf))
        if res.lower().startswith('y'):
            if not os.path.isfile(target + '.orig'):
                shutil.move(target, target+'.orig')
                print('{} moved to {}.orig'.format(target, target))
            else: sys.exit('rename some stuff on your own')

    if os.path.isfile(target):
        os.remove(target)
    elif os.path.islink(target):
        os.unlink(target)


def link_file(conf):
    '''
    This function calls a helper function that allows the user to save
    old configuration files before they are replaced new files via
    symbolic link. The only parameter is the name of the confile being added.
    '''
    target = os.path.join(HOME_DIR, conf)
    if os.path.exists(target):
        check_with_user(target)
    os.symlink(os.path.join(CONF_DIR, conf), target)


def main():
    '''
    I changed everything because it was weirdly complicated before
    '''

    # for parsing command line args
    parser = argparse.ArgumentParser()

    parser.add_argument('targets', nargs='+',
                        help='path to tmux config file if not default')
    parser.add_argument('-a', '--all_files', default=False, action='store_true',
                        help='install all available config files')

    args = parser.parse_args()
    targets = CONF_FILES if args.all_files else args.targets
    for conf in targets:
        if os.path.exists(conf):
            link_file(conf)
        else:
            print('ignoring {}'.format(conf))

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt as error:
        sys.exit('\nexiting...')
