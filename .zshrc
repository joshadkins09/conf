#===========================================================================#
# Author: Josh Adkins                                                       #
# Title : .zshrc                                                            #
# Date  : 21 January 2013                                                   #
#===========================================================================#

# general unix stuff
alias rm='rm -i'
alias ssh='ssh -e :'
export EDITOR='emacs'
export SVN_EDITOR='emacs'

alias exip='curl http://ipecho.net/plain; echo'

# exitOnFailure() {
#     $@
#     if [ $? -ne 0 ]; then
#         echo "There was an error with the last command:" $@
#         exit $?
#     fi
# }

# binExists() {
#     if [ $# -gt 0 ]; then
# 		if [ "`which $1`" == "" ]
# 		then echo "----> $1 is not installed" 
# 			echo "----> installing $1"
# 			apt-get install $1 -y
# 			echo "----> $1 installed"
# 		else echo "----> $1 is already installed"
# 		fi
#     else
# 		echo "not enough args"
# 		exit
#     fi
# }

# linux only
if [ $(uname) = 'Linux' ]; then
	export COLOREXT=' --color=auto'
	alias reap='sudo service apache2 restart'
	alias na='nautilus'
	alias emacs='emacs -nw'
	laptop-detect
	if [ $? -eq 0 ]; then
		synclient FingerLow=0\
          FingerHigh=0\
          HorizTwoFingerScroll=1\
          VertTwoFingerScroll=1
		alias touchpad='source ~/.touchpad'
	fi
elif [ $(uname) = 'Darwin' ]; then
	export COLOREXT=' -G'

	# requires macports
	alias apachectl='/opt/local/apache2/bin/apachectl'
	export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/opt/local/bin
	alias reap='sudo apachectl restart'
	# if [ -d /opt/local/bin ]; then alias emacs='/opt/local/bin/emacs'; fi
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias egrep='egrep'$COLOREXT
    alias fgrep='fgrep'$COLOREXT
    alias  grep='grep'$COLOREXT
fi

alias ls='ls'$COLOREXT

autoload -U colors && colors
PS1="%{$fg[blue]%}%n%{$reset_color%}@%{$fg[blue]%}%m: %{$fg[blue]%}%{$reset_color%}% "
