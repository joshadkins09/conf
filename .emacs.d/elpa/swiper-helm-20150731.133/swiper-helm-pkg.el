(define-package "swiper-helm" "20150731.133" "Helm version of Swiper." '((emacs "24.1") (swiper "0.1.0") (helm "1.5.3")) :url "https://github.com/abo-abo/swiper-helm" :keywords '("matching"))
