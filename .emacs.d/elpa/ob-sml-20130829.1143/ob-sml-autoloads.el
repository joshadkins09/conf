;;; ob-sml-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "ob-sml" "ob-sml.el" (21783 34262 0 0))
;;; Generated autoloads from ob-sml.el

(autoload 'org-babel-execute:sml "ob-sml" "\
Execute a block of Standard ML code with org-babel.  This function is
called by `org-babel-execute-src-block'

\(fn BODY PARAMS)" nil nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; ob-sml-autoloads.el ends here
