(define-package "ob-sml" "20130829.1143" "org-babel functions for template evaluation" '((sml-mode "6.4")) :url "http://orgmode.org" :keywords '("literate programming" "reproducible research"))
