(define-package "term-run" "20150601.6" "Run arbitrary command in terminal buffer" 'nil :url "https://github.com/10sr/term-run-el" :keywords '("utility" "shell" "command" "term-mode"))
