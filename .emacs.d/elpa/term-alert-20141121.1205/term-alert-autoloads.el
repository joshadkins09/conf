;;; term-alert-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "term-alert" "term-alert.el" (21742 50667 0
;;;;;;  0))
;;; Generated autoloads from term-alert.el

(autoload 'term-alert-next-command-toggle "term-alert" "\
Toggle whether to display an alert when a command next completes in this buffer.  If NUM is equal to `term-alert-count', disable Term Alert mode.  With prefix arg, alert for that number of commands.

\(fn NUM)" t nil)

(autoload 'term-alert-all-toggle "term-alert" "\
Toggle whether to display an alert after all commands until further notice.

\(fn)" t nil)

(autoload 'term-alert-callback "term-alert" "\
Respond to a completed command.  C and A are unused.

\(fn C A)" nil nil)

(add-to-list 'term-cmd-commands-alist '("term-alert-done" . term-alert-callback))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; term-alert-autoloads.el ends here
