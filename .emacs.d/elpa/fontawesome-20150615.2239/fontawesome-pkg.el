(define-package "fontawesome" "20150615.2239" "fontawesome utility"
  '((helm "1.7.2")
    (cl-lib "0.5"))
  :url "https://github.com/syohex/emacs-fontawesome")
;; Local Variables:
;; no-byte-compile: t
;; End:
