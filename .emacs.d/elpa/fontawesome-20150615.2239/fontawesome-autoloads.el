;;; fontawesome-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "fontawesome" "fontawesome.el" (21908 45938
;;;;;;  0 0))
;;; Generated autoloads from fontawesome.el

(autoload 'fontawesome "fontawesome" "\
Return fontawesome code point

\(fn FONT-NAME)" t nil)

(autoload 'helm-fontawesome "fontawesome" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("fontawesome-data.el" "fontawesome-pkg.el")
;;;;;;  (21908 45938 585462 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; fontawesome-autoloads.el ends here
