(define-package "emms-player-mpv-jp-radios" "20150728.2128" "EMMS players and stream lists of Japan radio stations"
  '((emacs "24")
    (cl-lib "0.5")
    (emms "4.0")
    (emms-player-simple-mpv "0.1.7"))
  :url "https://github.com/momomo5717/emms-player-mpv-jp-radios" :keywords
  '("emms" "mpv" "radio"))
;; Local Variables:
;; no-byte-compile: t
;; End:
