;;; emms-player-mpv-jp-radios-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "emms-player-mpv-jp-radios" "emms-player-mpv-jp-radios.el"
;;;;;;  (21949 6866 0 0))
;;; Generated autoloads from emms-player-mpv-jp-radios.el

(autoload 'emms-player-mpv-jp-radios-add "emms-player-mpv-jp-radios" "\
Add emms simple players for NAMES to `emms-player-list'.

\(fn &rest NAMES)" nil nil)

(autoload 'emms-player-mpv-jp-radios-add-all "emms-player-mpv-jp-radios" "\
Add all EMMS jp radio players to `emms-player-list'.

\(fn)" nil nil)

;;;***

;;;### (autoloads nil nil ("emms-player-mpv-jp-radios-pkg.el") (21949
;;;;;;  6866 380622 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; emms-player-mpv-jp-radios-autoloads.el ends here
