;;; django-snippets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (django-snippets-initialize) "django-snippets"
;;;;;;  "django-snippets.el" (21345 24937 0 0))
;;; Generated autoloads from django-snippets.el

(autoload 'django-snippets-initialize "django-snippets" "\


\(fn)" nil nil)

(eval-after-load 'yasnippet '(django-snippets-initialize))

;;;***

;;;### (autoloads nil nil ("django-snippets-pkg.el") (21345 24937
;;;;;;  969239 0))

;;;***

(provide 'django-snippets-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; django-snippets-autoloads.el ends here
