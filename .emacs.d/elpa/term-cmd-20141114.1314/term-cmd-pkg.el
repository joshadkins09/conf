(define-package "term-cmd" "20141114.1314" "Send commands to Emacs from programs running under term.el" 'nil :url "https://github.com/CallumCameron/term-cmd" :keywords '("processes"))
