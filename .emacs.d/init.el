;;; package --- emacs config file
;;; Commentary:

;;==============================================================================
;; Author: Josh Adkins
;; Title : .emacs                      | _  _|_   /\  _|| o._  _
;; Date  : 25 June 2012              \_|(_)_>| | /--\(_||<|| |_>
;; Copyright (C) 2012
;;==============================================================================

;;; Code:
;;------------------------------------------------------------------------------
;; loading setup
;;------------------------------------------------------------------------------

(add-to-list 'load-path "~/.emacs.d/src")

(require 'custom-functions)
(require 'custom-modes)
(require 'custom-set)
(require 'gud-setup)
(require 'key-bindings)
(require 'lilypond)
(require 'prog-mode-setup)

;;------------------------------------------------------------------------------
;; emacs > 24 only
;;------------------------------------------------------------------------------

(when (>= emacs-major-version 24)
  (require 'package-setup)
  (require 'expand-region)
  (require 'flycheck-google-cpplint)
  (require 'flycheck-tip)
  (require 'google-c-style)
  (require 'ox-reveal)
  (require 'paredit-setup)
  (require 'ac-setup)
  (require 'ace-setup)
  (require 'appearance-setup)
  (require 'c-setup)
  (require 'eshell-setup)
  (require 'helm-setup)
  (require 'key-chord-setup)
  (require 'mu4e-setup)
  (require 'multiple-cursors-setup)
  (require 'projectile-setup)
  (require 'python-setup)
  (require 'tex-setup)
  (require 'yasnippet-setup))
(provide '.emacs)
