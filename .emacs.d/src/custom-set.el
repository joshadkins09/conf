;;==============================================================================
;; custom-set-variables
;;==============================================================================

(custom-set-variables
 '(auto-save-default nil)
 '(backup-inhibited t t)
 '(column-number-mode t)
 '(cua-enable-cua-keys nil)
 '(cua-mode t nil (cua-base))
 '(custom-safe-themes
   (quote
    ("83279c1d867646c5eea8a804a67a23e581b9b3b67f007e7831279ed3a4de9466"
     "6ebb2401451dc6d01cd761eef8fe24812a57793c5ccc427b600893fa1d767b1d"
     default)))
 '(display-time-day-and-date t)
 '(display-time-mode t)
 '(fill-column 79)
 '(linum-format "%d ")
  '(global-font-lock-mode t)
 '(inhibit-startup-screen t)
 '(ispell-program-name "/usr/local/bin/ispell")
 '(save-place t nil (saveplace))
 '(send-mail-function (quote smtpmail-send-it))
 '(sentence-end "[.?!][]\"')}]*\\($\\|[ \t]\\)[ \t\n]*")
 '(sentence-end-double-space t)
 '(show-paren-mode t nil (paren))
 '(transient-mark-mode t)
 )

(put 'narrow-to-region 'disabled nil)
;; (set-frame-font "Inconsolata for Powerline-12" nil t)
;; (setq exec-path '("/usr/local/bin" "/usr/bin" "/bin" "/usr/sbin" "/sbin"
;; 		  "/opt/X11/bin" "/usr/texbin" "/usr/local/sbin"
;; 		  "/usr/local/texlive/2014/bin/universal-darwin"))

;; this is dumb -jadkins
;; (setenv "PATH" "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/texbin:/usr/local/sbin:/usr/local/texlive/2014/bin/universal-darwin")

(provide 'custom-set)
