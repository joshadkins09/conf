;;==============================================================================
;; custom functions							       
;;==============================================================================
;; .emacs file 
;;------------------------------------------------------------------------------

(defun reload-dot-emacs ()
  "This command reloads .emacs file to reflect latest file contents."
  (interactive)
  (load-file "~/.emacs.d/init.el"))

(defun edit-dot-emacs ()
  "This command opens the .emacs file for editing."
  (interactive)
  (find-file "~/.emacs.d/init.el"))

;;------------------------------------------------------------------------------
;; revert all buffers
;;------------------------------------------------------------------------------

(defun revert-all-buffers ()
  "Refreshes all open buffers from their respective files."
  (interactive)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when (and (buffer-file-name) (file-exists-p (buffer-file-name)) (not (buffer-modified-p)))
	(revert-buffer t t t))))
  (message "Refreshed open files."))

;;------------------------------------------------------------------------------
;; uniqify buffers
;;------------------------------------------------------------------------------

(defun uniquify-all-lines-region (start end)
  "Find duplicate lines in region START to END keeping first occurrence."
  (interactive "*r")
  (save-excursion
    (let ((end (copy-marker end)))
      (while
	  (progn
	    (goto-char start)
	    (re-search-forward "^\\(.*\\)\n\\(\\(.*\n\\)*\\)\\1\n" end t))
	(replace-match "\\1\n\\2")))))

(defun uniquify-all-lines-buffer ()
  "Delete duplicate lines in buffer and keep first occurrence."
  (interactive "*")
  (uniquify-all-lines-region (point-min) (point-max)))

(add-to-list 'load-path "~/.emacs.d/extras")
(require 'cycle-buffer)
(require 'redo+)

;;------------------------------------------------------------------------------
;; functions I have written
;;------------------------------------------------------------------------------

(require 'separators)

(require 'custom-snippets)

(provide 'custom-functions)
