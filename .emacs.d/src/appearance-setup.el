;;==============================================================================
;; appearance-setup
;;==============================================================================

(load-theme 'base16-chalk-dark)
(menu-bar-mode -1)

(when window-system
  (progn
    (fringe-mode 0)
    (nyan-mode t)
    (scroll-bar-mode -1)
    (set-cursor-color "#ffffff")
    (setq default-cursor-type 'bar)
    (tool-bar-mode -1)))

(provide 'appearance-setup)
