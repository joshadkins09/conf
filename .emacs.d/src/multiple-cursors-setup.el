;;==============================================================================
;; multiple cursors setup
;;==============================================================================

(global-set-key (kbd "C-c m e") 'mc/edit-lines)
(global-set-key (kbd "C-c m n") 'mc/mark-next-like-this)
(global-set-key (kbd "C-c m p") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c m d") 'mc/unmark-next-like-this)
(global-set-key (kbd "C-c m u") 'mc/unmark-previous-like-this)
(global-set-key (kbd "C-c m s") 'mc/mark-next-symbol-like-this)
(global-set-key (kbd "C-c m r") 'mc/mark-all-in-region)
(global-set-key (kbd "C-c m g") 'mc/mark-all-like-this)
(global-set-key (kbd "C-c m i") 'mc/insert-numbers)

(provide 'multiple-cursors-setup)
