;;==============================================================================
;; custom snippets
;;==============================================================================

(defun cc-insert-accessor ()
  (interactive)
  (let ((type nil)
	(name nil))
    (setq
     type (read-string "variable type: ")
     name (read-string "variable name: "))
    (insert (format "%s %s() const {" type name))
    (reindent-then-newline-and-indent)
    (insert (format "return %s_;" name))
    (reindent-then-newline-and-indent)
    (insert "}")
    (reindent-then-newline-and-indent)))

(defun cc-insert-mutator ()
  (interactive)
  (let ((type nil)
	(name nil))
    (setq
     type (read-string "variable type: ")
     name (read-string "variable name: "))
    (insert (format "void set_%s(%s %s) {" name type name ))
    (reindent-then-newline-and-indent)
    (insert (format "%s_ = %s;" name name))
    (reindent-then-newline-and-indent)
    (insert "}")
    (reindent-then-newline-and-indent)))

(defun cc-insert-getset ()
  (interactive)
  (let ((type nil)
	(name nil))
    (setq
     type (read-string "variable type: ")
     name (read-string "variable name: "))
    (insert (format "%s %s() const {" type name))
    (reindent-then-newline-and-indent)
    (insert (format "return %s_;" name))
    (reindent-then-newline-and-indent)
    (insert "}")
    (reindent-then-newline-and-indent)
    (reindent-then-newline-and-indent)
    (insert (format "void set_%s(%s %s) {" name type name ))
    (reindent-then-newline-and-indent)
    (insert (format "%s_ = %s;" name name))
    (reindent-then-newline-and-indent)
    (insert "}")
    (reindent-then-newline-and-indent)))

(global-set-key (kbd "C-c i a") 'cc-insert-accessor)
(global-set-key (kbd "C-c i m") 'cc-insert-mutator)
(global-set-key (kbd "C-c i b") 'cc-insert-getset)

(provide 'custom-snippets)
