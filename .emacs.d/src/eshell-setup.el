;;==============================================================================
;; eshell setup
;;==============================================================================

;; (defmacro with-face (str &rest properties)
;;   `(propertize ,str 'face (list ,@properties)))

;; (defun shk-eshell-prompt ()
;;   (let ((header-bg "#fff"))
;;     (concat
;;      ;; (with-face (concat (eshell/pwd) " ") :background header-bg)
;;      ;; (with-face (format-time-string "(%Y-%m-%d %H:%M) "(current-time)) :background header-bg :foreground "#888")
;;      ;; (with-face
;;      ;;  (or (ignore-errors (format "(%s)" (vc-responsible-backend default-directory))) "")
;;      ;;  :background header-bg)
;;      ;; (with-face "\n" :background header-bg)
;;      (with-face user-login-name :foreground (face-foreground font-lock-function-name-face))
;;      "@"
;;      (with-face (replace-regexp-in-string "\.local$" "" system-name)
;; 		:foreground (face-foreground font-lock-negation-char-face))
;;      (with-face " -->" :foreground (face-foreground font-lock-keyword-face))
;;      " ")))

;; (setq eshell-prompt-function 'shk-eshell-prompt)
;; (setq eshell-highlight-prompt nil)

(add-hook 'writeroom-mode-hook 'auto-fill-mode)

;;------------------------------------------------------------------------------
;; helm eshell
;;------------------------------------------------------------------------------

(require 'helm-eshell)

(add-hook 'eshell-mode-hook
          #'(lambda ()
              (define-key eshell-mode-map (kbd "M-l")  'helm-eshell-history)))

;; for shell-mode
(define-key shell-mode-map (kbd "C-c C-l") 'helm-comint-input-ring)

(provide 'eshell-setup)
