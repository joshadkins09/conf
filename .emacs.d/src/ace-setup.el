;;==============================================================================
;; ace setup
;;==============================================================================

(global-set-key (kbd "M-o") 'ace-window)
(global-set-key (kbd "M-j") 'ace-jump-buffer)
(global-set-key (kbd "C-c SPC") 'ace-jump-mode)

(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))

(key-chord-define-global "jj" 'ace-jump-word-mode)

(provide 'ace-setup)
