;;==============================================================================
;; paredit
;;==============================================================================

(autoload 'enable-paredit-mode
  "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook                  #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook                        #'enable-paredit-mode)
(add-hook 'lisp-mode-hook                        #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook            #'enable-paredit-mode)
(add-hook 'scheme-mode-hook                      #'enable-paredit-mode)
(add-hook 'c-mode-hook                           #'enable-paredit-mode)
(add-hook 'c++-mode-hook                         #'enable-paredit-mode)
(add-hook 'python-mode-hook                      #'enable-paredit-mode)
(add-hook 'php-mode-hook                         #'enable-paredit-mode)

;; fix paredit spacing
(eval-after-load "paredit"
  '(defun paredit-space-for-delimiter-p (endp delimiter)
     (and (not (if endp (eobp) (bobp)))
          (memq (char-syntax (if endp (char-after) (char-before)))
                (list ?\"
                      (let ((matching (matching-paren delimiter)))
                        (and matching (char-syntax matching))))))))

;; key bindings
(global-set-key (kbd "C-c j e") 'paredit-mode)
(global-set-key (kbd "C-c j w s") 'paredit-wrap-square)
(global-set-key (kbd "C-c j w a") 'paredit-wrap-angled)
(global-set-key (kbd "C-c j w c") 'paredit-wrap-curly)

(provide 'paredit-setup)
