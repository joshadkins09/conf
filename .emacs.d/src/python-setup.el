;;==============================================================================
;; elpy                                                                         
;;==============================================================================

;; (defun my-elpy-init ()

;; enable elpy mode
(elpy-enable)

;; fixing a key binding bug in elpy
(define-key yas-minor-mode-map (kbd "C-c k") 'yas-expand)

;; turn off highlight indentation
(add-hook 'python-mode-hook highlight-indentation-mode)

(add-hook 'python-mode-hook 'jedi:setup)

(setq jedi:setup-keys t)
(setq jedi:complete-on-dot t)

;; )
;; (add-hook 'python-mode-hook 'my-elpy-init)

;;------------------------------------------------------------------------------
;; iPython                                                                      
;;------------------------------------------------------------------------------

(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args ""
 python-shell-prompt-regexp "In \\[[0-9]+\\]: "
 python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
 python-shell-completion-setup-code
 "from IPython.core.completerlib import module_completion"
 python-shell-completion-module-string-code
 "';'.join(module_completion('''%s'''))\n"
 python-shell-completion-string-code
 "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")

(add-hook 'python-mode-hook #'(lambda () (setq flycheck-checker 'python-pylint)))
(add-hook 'python-mode-hook #'(lambda () (setq flycheck-checker 'python-pylint)))

(provide 'python-setup)
