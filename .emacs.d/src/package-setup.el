;;==============================================================================
;; package setup
;;==============================================================================

(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/")
	     ;; '("marmalade" . "http://marmalade-repo.org/packages/")
	     )
(package-initialize)

(provide 'package-setup)
