;;==============================================================================
;; custom key bindings
;;==============================================================================

;; cycle buffer
(global-set-key (kbd "C-x p") 'cycle-buffer-backward)
(global-set-key (kbd "C-x n") 'cycle-buffer)

;; ibuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; redo
(global-set-key (kbd "M-/") 'redo)

;; remap meta-x
(global-set-key (kbd "C-x C-m") 'execute-extended-command)

;; window resize
(global-set-key (kbd "M-7") 'shrink-window-horizontally)
(global-set-key (kbd "M-0") 'enlarge-window-horizontally)
(global-set-key (kbd "M-8") 'shrink-window)
(global-set-key (kbd "M-9") 'enlarge-window)

;; region formatting
(global-set-key (kbd "C-c c l") 'center-line)
(global-set-key (kbd "C-c n r") 'narrow-to-region)
(global-set-key (kbd "C-c w r") 'widen)

;; go to line
(global-set-key (kbd "M-g") 'goto-line)

;; toggle line numbers
(global-set-key (kbd "C-c l n") 'linum-mode)

;; cua mark rectangle
(global-set-key (kbd "C-c s") 'cua-set-rectangle-mark)

;; replace string
(global-set-key (kbd "C-c r s") 'replace-string)
(global-set-key (kbd "C-c r r") 'replace-regexp)

;; iedit-mode
(global-set-key (kbd "C-c i e") 'iedit-mode)

;; align regexp
(global-set-key (kbd "C-x \\") 'align-regexp)

;; toggle read only
(global-set-key (kbd "C-c r o") 'read-only-mode)

;; word count
(global-set-key (kbd "C-c c w") 'count-words)

;; toggle flycheck
(global-set-key (kbd "C-c f c") 'flycheck-mode)

;; delete trailing whitespace
(global-set-key (kbd "C-c t w") 'delete-trailing-whitespace)

;; untabify
(global-set-key (kbd "C-c u t") 'untabify)

;; whitespace mode
(global-set-key (kbd "C-c w m") 'whitespace-mode)

;; hs
(global-set-key (kbd "C-c f s a") 'hs-show-all)
(global-set-key (kbd "C-c f h a") 'hs-hide-all)
(global-set-key (kbd "C-c f s b") 'hs-show-block)
(global-set-key (kbd "C-c f h b") 'hs-hide-block)
(global-set-key (kbd "C-c f t") 'hs-toggle-hiding)

;; compile
(global-set-key (kbd "C-c C-c") 'compile)

;; edit/reload .emacs
(global-set-key (kbd "C-c r e") 'reload-dot-emacs)
(global-set-key (kbd "C-c e e") 'edit-dot-emacs)
(global-set-key (kbd "C-c e b") 'eval-buffer)

;; revert all buffers visiting file
(global-set-key (kbd "C-c r a") 'revert-all-buffers)

;; package
(global-set-key (kbd "C-c l p") 'package-list-packages)

;; expand region
(global-set-key (kbd "C-x j") 'er/expand-region)
(setq expand-region-contract-fast-key "k")

;; golden ratio
(global-set-key (kbd "C-c g r") 'golden-ratio-mode)

;; help map
(global-set-key (kbd "C-c h f") 'describe-function)
(global-set-key (kbd "C-c h k") 'describe-key)
(global-set-key (kbd "C-c h w") 'where-is)

;; eshell
(global-set-key (kbd "C-c e s") 'eshell)

;; magit
(global-set-key (kbd "C-x g") 'magit-status)

;; set c basic offset
(defun set-c-basic-offset-4 ()
  (interactive)
  (setq c-basic-offset 4))
(global-set-key (kbd "C-c b o") 'set-c-basic-offset-4)

(provide 'key-bindings)
