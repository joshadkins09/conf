;;==============================================================================
;; c/c++ setup
;;==============================================================================

(defun my:ac-c-header-init ()
  (require 'auto-complete-c-headers)
  (add-to-list 'ac-sources 'ac-source-c-headers)  )

(eval-after-load "cc-mode"
  '(progn
     (define-key c-mode-map (kbd "C-c C-c") 'compile)
     (define-key c++-mode-map (kbd "C-c C-c") 'compile)))

;; call this function from c/c++ hooks
(add-hook 'c-mode-common-hook 'my:ac-c-header-init)
(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'google-make-newline-indent)

(provide 'c-setup)
