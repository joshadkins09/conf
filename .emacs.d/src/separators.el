;;==============================================================================
;; separators
;;------------------------------------------------------------------------------
;; separator line
;;------------------------------------------------------------------------------

(defun format-separator-line (char)
  (let ((prefix (remove ?  comment-start)))
    (setq prefix (concat prefix (make-string comment-add (string-to-char comment-start))))
    (concat prefix (make-string (+ 1 (- fill-column (length prefix))) char))))

(defun separator-line (char)
  (interactive
   (let ((char (read-char "char for line: ")))
     (list (insert (format-separator-line char))))))

(defun single-line () (interactive) (insert (format-separator-line ?-)))
(defun double-line () (interactive) (insert (format-separator-line ?=)))

;;------------------------------------------------------------------------------
;; separator block
;;------------------------------------------------------------------------------

(defun format-separator-block (char heading)
  (let ((prefix (remove ?  comment-start)))
    (setq prefix (concat prefix (make-string comment-add (string-to-char comment-start))))
    (format "%s\n%s\n%s\n\n"
	    (format-separator-line char)
	    (concat prefix comment-padding heading )
	    (format-separator-line char))))

(defun separator-block (char heading)
  (interactive
   (let ((char (read-char "char for block: "))
	 (heading (read-string "heading: ")))
     (list (format-separator-block char heading)))))

(defun single-block ()
  (interactive
   (let ((heading (read-string "heading: ")))
     (insert (format-separator-block ?- heading)))))

(defun double-block ()
  (interactive
   (let ((heading (read-string "heading: ")))
     (insert (format-separator-block ?= heading)))))

;;------------------------------------------------------------------------------
;; key bindings
;;------------------------------------------------------------------------------

(global-set-key (kbd "C-c l s") 'single-line)
(global-set-key (kbd "C-c l d") 'double-line)

;; days are numbered on these bindings -jadkins
(global-set-key (kbd "C-c b s") 'single-block)
(global-set-key (kbd "C-c b d") 'double-block)

(provide 'separators)
