;;==============================================================================
;; helm-setup
;;==============================================================================

(require 'helm)
(require 'helm-config)

(when (executable-find "ack-grep")
  (setq
   helm-grep-default-command "ack-grep -Hn --no-group --no-color %e %p %f"
   helm-grep-default-recurse-command "ack-grep -H --no-group --no-color %e %p %f"))

(add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

;;------------------------------------------------------------------------------
;; key bindings
;;------------------------------------------------------------------------------

(global-set-key (kbd "C-c h m") 'helm-mode)
(global-set-key (kbd "C-h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
(global-set-key (kbd "C-h o") 'helm-occur)
(global-set-key (kbd "C-h x") 'helm-register)
(global-set-key (kbd "C-h g") 'helm-do-grep)
(global-set-key (kbd "C-h c") 'helm-flycheck)
(global-set-key (kbd "C-h w") 'helm-colors)

;; rebind tab to do persistent action
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)

;; make TAB works in terminal
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)

;; list actions using C-z
(define-key helm-map (kbd "C-z")  'helm-select-action)

;;------------------------------------------------------------------------------
;; helm gtags mode
;;------------------------------------------------------------------------------

(add-hook 'asm-mode-hook 'helm-gtags-mode)
(add-hook 'c-mode-hook 'helm-gtags-mode)
(add-hook 'c++-mode-hook 'helm-gtags-mode)

;;------------------------------------------------------------------------------
;; helm swoop
;;------------------------------------------------------------------------------

(require 'helm-swoop)

;; Change the keybinds to whatever you like :)
(global-set-key (kbd "M-i") 'helm-swoop)
(global-set-key (kbd "M-I") 'helm-swoop-back-to-last-point)
(global-set-key (kbd "C-c M-i") 'helm-multi-swoop)
(global-set-key (kbd "C-x M-i") 'helm-multi-swoop-all)

;; When doing isearch, hand the word over to helm-swoop
(define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
;; From helm-swoop to helm-multi-swoop-all
(define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)
;; When doing evil-search, hand the word over to helm-swoop
;; (define-key evil-motion-state-map (kbd "M-i") 'helm-swoop-from-evil-search)

;; Move up and down like isearch
(define-key helm-swoop-map (kbd "C-r") 'helm-previous-line)
(define-key helm-swoop-map (kbd "C-s") 'helm-next-line)
(define-key helm-multi-swoop-map (kbd "C-r") 'helm-previous-line)
(define-key helm-multi-swoop-map (kbd "C-s") 'helm-next-line)

;; Save buffer when helm-multi-swoop-edit complete
(setq helm-multi-swoop-edit-save t)

;; If this value is t, split window inside the current window
(setq helm-swoop-split-with-multiple-windows nil)

;; Split direcion. 'split-window-vertically or 'split-window-horizontally
(setq helm-swoop-split-direction 'split-window-vertically)

;; If nil, you can slightly boost invoke speed in exchange for text color
(setq helm-swoop-speed-or-color nil)

;; ;; Go to the opposite side of line from the end or beginning of line
(setq helm-swoop-move-to-line-cycle t)

;; Optional face for line numbers
;; Face name is `helm-swoop-line-number-face`
(setq helm-swoop-use-line-number-face t)

;;------------------------------------------------------------------------------
;; custom set
;;------------------------------------------------------------------------------

(custom-set-variables
 '(helm-gtags-prefix-key (kbd "C-c t"))
 '(helm-gtags-suggested-key-mapping t)
 '(helm-gtags-path-style 'relative)
 '(helm-gtags-ignore-case t)
 '(helm-gtags-auto-update t)
 '(helm-scroll-amount 8)
 '(helm-split-window-in-side-p t)
 '(helm-move-to-line-cycle-in-source t)
 '(helm-ff-search-library-in-sexp t)
 '(helm-ff-file-name-history-use-recentf t)
 '(helm-autoresize-mode t)
 '(helm-apropos-fuzzy-match t)
 '(helm-buffers-fuzzy-matching t)
 '(helm-imenu-fuzzy-match t)
 '(helm-lisp-fuzzy-completion t)
 '(helm-locate-fuzzy-match t)
 '(helm-M-x-fuzzy-match t)
 '(helm-recentf-fuzzy-match t)
 '(helm-semantic-fuzzy-match t))

(helm-mode 1)
(provide 'helm-setup)
