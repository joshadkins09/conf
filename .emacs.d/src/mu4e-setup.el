;;==============================================================================
;; mu4e									       
;;==============================================================================

(add-to-list 'load-path "~/.emacs.d/mu4e")
(require 'mu4e)

;; default
(setq mu4e-maildir "~/.mail/joshadkins09")

(setq mu4e-drafts-folder "/drafts")
(setq mu4e-sent-folder   "/sent")
(setq mu4e-trash-folder  "/trash")

;; don't save message to Sent Messages, Gmail/IMAP takes care of this
(setq mu4e-sent-messages-behavior 'delete)

;; (See the documentation for `mu4e-sent-messages-behavior' if you have
;; additional non-Gmail addresses and want assign them different
;; behavior.)

;; setup some handy shortcuts
;; you can quickly switch to your Inbox -- press ``ji''
;; then, when you want archive some messages, move them to
;; the 'All Mail' folder by pressing ``ma''.

(setq mu4e-maildir-shortcuts
	'( ("/Inbox"          . ?i)
	   ("/sent"           . ?s)
	   ("/trash"          . ?t)
	   ("/archive"        . ?a)))

;; allow for updating mail using 'U' in the main view:
(setq mu4e-get-mail-command "offlineimap")

(setq mu4e-mu-binary "/usr/local/bin/mu")

;; something about ourselves
(setq
 user-mail-address "joshadkins09@gmail.com"
 user-full-name  "Josh Adkins"
 mu4e-compose-signature
 (concat
  "Josh Adkins\n"
  "\n"))

;; alternatively, for emacs-24 you can use:
(setq message-send-mail-function 'smtpmail-send-it
    smtpmail-stream-type 'starttls
    smtpmail-default-smtp-server "smtp.gmail.com"
    smtpmail-smtp-server "smtp.gmail.com"
    smtpmail-smtp-service 587)

;; don't keep message buffers around
(setq message-kill-buffer-on-exit t)

;; key bindings
(global-set-key (kbd "C-c e m") 'mu4e)
(global-set-key (kbd "C-c e r") 'mu4e-compose-reply)

(provide 'mu4e-setup)
