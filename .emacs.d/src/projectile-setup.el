;;==============================================================================
;; projectile setup
;;=================-------------------------------------------------------------

(setq
 projectile-completion-system 'helm
 projectile-switch-project-action 'helm-projectile-find-file
 projectile-switch-project-action 'helm-projectile
 projectile-enable-caching t
 )

(projectile-global-mode)
(helm-projectile-on)
(provide 'projectile-setup)
