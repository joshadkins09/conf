; Enable flycheck for all buffers
; (add-hook 'after-init-hook #'global-flycheck-mode)
  (setq flycheck-highlighting-mode 'lines)
  
  (eval-after-load 'flycheck
    '(progn
       
       (flycheck-add-next-checker 'c/c++-cppcheck
  				  'c/c++-googlelint 'append)))


; (define-key your-prog-mode (kbd "C-c C-n") 'flycheck-tip-cycle)
  (flycheck-tip-use-timer 'verbose)
