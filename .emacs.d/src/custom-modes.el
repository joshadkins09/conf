;;==============================================================================
;; languages/frameworks                                                         
;;==============================================================================

;; c++ mode for header files
(add-to-list 'auto-mode-alist '("\\.h$" . c++-mode))

;; set org mode for csv files
(add-to-list 'auto-mode-alist '("\\.csv$" . org-mode))

(setq auto-mode-alist
      (append '(("\\.rst$" . rst-mode)
                ("\\.rest$" . rst-mode)) auto-mode-alist))

;; shell mode for .customrc
(add-to-list 'auto-mode-alist '("\\.customrc$" . shell-script-mode))

;;------------------------------------------------------------------------------
;; write room mode
;;------------------------------------------------------------------------------

(setq auto-mode-alist
      (append '(("\\.txt$" . writeroom-mode)) auto-mode-alist))

(add-hook 'writeroom-mode-hook 'auto-fill-mode)

;;------------------------------------------------------------------------------
;; load python-mode for scons files
;;------------------------------------------------------------------------------

(setq compile-command "/usr/local/bin/scons")

(setq auto-mode-alist(cons '("SConstruct" . python-mode) auto-mode-alist))
(setq auto-mode-alist(cons '("SConscript" . python-mode) auto-mode-alist))

 (setq auto-mode-alist
      (cons '("SConstruct" . python-mode) auto-mode-alist))
 (setq auto-mode-alist
      (cons '("SConscript" . python-mode) auto-mode-alist))

(provide 'custom-modes)
