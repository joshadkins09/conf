;;==============================================================================
;; auto-complete setup
;;==============================================================================

(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)
(provide 'ac-setup)
