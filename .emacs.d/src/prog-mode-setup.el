;;==============================================================================
;; prog mode
;;==============================================================================

;; rainbow delimiters
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;; hs-minor-mode for code folding
(add-hook 'prog-mode-hook 'hs-minor-mode)

;; hl-line-mode
(add-hook 'prog-mode-hook 'hl-line-mode)

(provide 'prog-mode-setup)
